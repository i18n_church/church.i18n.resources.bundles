module church.i18n.resources.bundles {
  requires static org.jetbrains.annotations;
  requires transitive org.slf4j;

  exports church.i18n.resources.bundles;
}