/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.resources.bundles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extension of a simple resource bundle able to load, and work with multiple message resource files.
 */
public class SingleLocaleMultiResourceBundle extends ResourceBundle implements MultiResourceBundle {

  private static final @NotNull Logger log = LoggerFactory.getLogger(SingleLocaleMultiResourceBundle.class);
  private final @NotNull List<ResourceBundle> bundles = new ArrayList<>();
  private final @NotNull Locale locale;

  /**
   * Creates an instance of this class without reading any message resource files.
   *
   * @param locale A locale of this resource bundle.
   */
  public SingleLocaleMultiResourceBundle(final @NotNull Locale locale) {
    super();
    this.locale = locale;
  }

  @Override
  public @NotNull MultiResourceBundle addMessageSources(final @Nullable ResourceBundleDescriptor... resourceBundleDescriptors) {
    log.trace("addMessageSources(fileLocations = [{}])", (Object[]) resourceBundleDescriptors);
    if (resourceBundleDescriptors == null) {
      return this;
    }
    for (ResourceBundleDescriptor bundleDescriptor : resourceBundleDescriptors) {
      if (bundleDescriptor != null && bundleDescriptor.resourceBundleName() != null) {
        //Since we have a modular application, we need to specify module from where we want to read properties file
        Module module = (bundleDescriptor.module() == null || bundleDescriptor.module().isEmpty())
                        ? ClassLoader.getSystemClassLoader().getUnnamedModule()
                        : ModuleLayer.boot().findModule(bundleDescriptor.module()).orElseGet(() ->
                            ClassLoader.getSystemClassLoader().getUnnamedModule()
                        );

        ResourceBundle bundle = ResourceBundle.getBundle(bundleDescriptor.resourceBundleName(), this.locale, module);
        /*
        We need to make following check, because when ResourceBundle does not find a message with
        the requested locale, it makes a failover and selects default locale.
        We would like to avoid it, so we do not mix locales.
         */
        if (this.locale.equals(bundle.getLocale())) {
          this.bundles.add(bundle);
        } else {
          log.warn("Failed to load locale: {}", this.locale);
        }
      }
    }
    return this;
  }

  @Override
  public @NotNull Locale getLocale() {
    log.trace("getLocale()");
    return this.locale;
  }

  /**
   * Gets an object for the given key from this resource bundle. Returns {@code null} if this resource bundle does not contain an object for the given key. In the case multiple files has contained the same key, there is no guarantee of what message will be
   * returned.
   *
   * @param key The key for the desired object.
   * @return The object for the given key, or {@code null}.
   * @throws NullPointerException If {@code key} is {@code null}.
   */
  @Override
  protected @Nullable Object handleGetObject(final @NotNull String key) {
    log.trace("handleGetObject(key = [{}])", key);
    return this.bundles.stream()
        .filter(delegate -> delegate.containsKey(key))
        .map(delegate -> delegate.getObject(key))
        .findFirst().orElse(null);
  }

  /**
   * Returns an enumeration of the keys.
   *
   * @return An @{code Enumeration} of the keys contained in this @{code ResourceBundle} and its parent bundles.
   */
  @Override
  public @NotNull Enumeration<String> getKeys() {
    log.trace("getKeys()");
    return Collections.enumeration(this.bundles.stream()
        .filter(Objects::nonNull)
        .flatMap(delegate -> Collections.list(delegate.getKeys()).stream())
        .collect(Collectors.toSet()));
  }
}
