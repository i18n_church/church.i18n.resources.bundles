/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.resources.bundles;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Holder of different resource bundles for multiple languages. The class allows adding resource bundles 'on the fly' and loads locales lazily, when they are required.
 */
public class PolyglotMultiResourceBundle implements MultiResourceBundle {

  private static final @NotNull Logger log = LoggerFactory.getLogger(PolyglotMultiResourceBundle.class);
  private static final @NotNull Object singletonLock = new Object();
  private static PolyglotMultiResourceBundle singleton;
  private final @NotNull Map<Locale, SingleLocaleMultiResourceBundle> locales = new HashMap<>();
  private final @NotNull List<ResourceBundleDescriptor> resourceBundleDescriptors = new ArrayList<>();
  private final @NotNull ReadWriteLock lock = new ReentrantReadWriteLock();
  private @NotNull Locale defaultLocale;

  public PolyglotMultiResourceBundle(final @Nullable Locale defaultLocale,
      final @Nullable String... resourceBundleLocations) {
    this(
        defaultLocale,
        resourceBundleLocations == null
        ? new ResourceBundleDescriptor[0]
        : Arrays.stream(resourceBundleLocations)
            .map(ResourceBundleDescriptor::ofUnnamedModule)
            .toArray(ResourceBundleDescriptor[]::new)
    );
  }

  /**
   * Load multiple resource bundles with messages.
   *
   * @param defaultLocale             Set default locale that will be used by this class. If {@code null}, {@link Locale#getDefault()} is used instead.
   * @param resourceBundleDescriptors List of resource bundle names to load. If you do not specify any file, no message will be retrieved. You can freely add resource files later. The order of added resource bundles is respected during message lookup.
   */
  public PolyglotMultiResourceBundle(final @Nullable Locale defaultLocale,
      final @Nullable ResourceBundleDescriptor... resourceBundleDescriptors) {
    log.trace("PolyglotMultiResourceBundle(defaultLocale = [{}], resourceBundleDescriptors = [{}])",
        defaultLocale, resourceBundleDescriptors);
    this.lock.writeLock().lock();
    this.defaultLocale = Objects.requireNonNullElseGet(defaultLocale, Locale::getDefault);
    try {
      if (resourceBundleDescriptors != null) {
        this.resourceBundleDescriptors.addAll(Arrays.asList(resourceBundleDescriptors));
      }
      //By default, always load root locale — no language, no country.
      loadLocale(Locale.ROOT);
      loadLocale(this.defaultLocale);
    } finally {
      this.lock.writeLock().unlock();
    }
  }

  /**
   * Load multiple resource bundles with messages. When you call this constructor without any specific locale, the default locale {@link Locale#getDefault()} will be used.
   *
   * @param resourceBundleDescriptors List of resource bundles names to load. If you do not specify any file, no message will be retrieved. You can freely add resource files later. The order of added resource bundles is respected during message lookup.
   */
  public PolyglotMultiResourceBundle(final @Nullable ResourceBundleDescriptor... resourceBundleDescriptors) {
    this(Locale.getDefault(), resourceBundleDescriptors);
  }

  public PolyglotMultiResourceBundle(final @Nullable String... resourceBundleLocations) {
    this(Locale.getDefault(),
        resourceBundleLocations == null
        ? new ResourceBundleDescriptor[0]
        : Arrays.stream(resourceBundleLocations)
            .map(ResourceBundleDescriptor::ofUnnamedModule)
            .toArray(ResourceBundleDescriptor[]::new)
    );
  }

  public PolyglotMultiResourceBundle() {
    this(Locale.getDefault(), new ResourceBundleDescriptor[0]);
  }

  /**
   * Retrieve the first localized message. The order of locales is respected, and the first found localized message is returned. If the specified locale was not loaded yet, it tries to load messages with required locale first and then it looks for a message.
   * It always tries to load also the message from the default locale if none from specified one was found.
   *
   * @param key                Message code reference from resource bundle.
   * @param prioritizedLocales Ordered list of locales. If the enumeration is {@code null} or empty, default and root locale are used instead
   * @return {@link Optional#empty()} is returned if the code is {@code null}, or the message was not found in any locale in any loaded resource bundle. Otherwise, it returns the message and locale of the first locale and the first resource bundle file where
   *     the message was found.
   */
  public @NotNull Optional<MessageFormat> getString(final @Nullable String key,
      final @Nullable List<Locale> prioritizedLocales) {
    log.trace("getString(key = [{}], prioritizedLocales = [{}])", key, prioritizedLocales);
    if (key == null) {
      return Optional.empty();
    }
    List<Locale> searchLocales = new ArrayList<>();
    if (prioritizedLocales != null) {
      searchLocales.addAll(prioritizedLocales);
    }
    searchLocales.add(this.defaultLocale);
    searchLocales.add(Locale.ROOT);
    this.lock.writeLock().lock();
    try {
      return searchLocales.stream()
          .filter(Objects::nonNull)
          .map(locale -> getString(key, locale))
          .flatMap(Optional::stream)
          .findFirst();
    } finally {
      this.lock.writeLock().unlock();
    }
  }

  /**
   * Retrieve the first localized message. The order of locales is respected and the first found localized message is returned. If the specified locale was not loaded yet, it tries to load messages with required locale first and then it looks for a message. It
   * always tries to load also the message from the default locale if none from specified one was found.
   *
   * @param key     Message code reference from resource bundle.
   * @param locales Ordered list of locales. If the enumeration is {@code null} or empty, default and root locale are used instead
   * @return {@link Optional#empty()} is returned if the code is {@code null} or the message was not found in any locale in any loaded resource bundle. Otherwise it returns the message and locale in the first locale and the first resource bundle file found.
   */
  public @NotNull Optional<MessageFormat> getString(final @Nullable String key,
      final @Nullable Enumeration<Locale> locales) {
    log.trace("getString(key = [{}], locales = [{}])", key, locales);
    if (locales == null) {
      return getString(key, Collections.emptyList());
    } else {
      return getString(key, Collections.list(locales));
    }
  }

  /**
   * Retrieve a localized message from the message bundle. If the specified locale was not loaded yet, it tries to load messages first and then it looks for a message.
   *
   * @param key    Message code reference from resource bundle.
   * @param locale Locale to use for message lookup.
   * @return {@link Optional#empty()} is returned if the code is {@code null} or the message was not found in default locale in any loaded resource bundle. Otherwise, it returns the message in the default locale, and the first resource bundle file found.
   */
  public @NotNull Optional<MessageFormat> getString(final @Nullable String key,
      final @Nullable Locale locale) {
    log.trace("getString(key = [{}], locale = [{}])", key, locale);
    if (locale == null || key == null) {
      return Optional.empty();
    }
    if (this.locales.get(locale) == null) {
      loadLocale(locale);
    }
    this.lock.readLock().lock();
    try {
      try {
        String message = this.locales.get(locale).getString(key);
        return Optional.of(new MessageFormat(message, locale));
      } catch (MissingResourceException ex) {
        log.info("Bundle cannot find a key [{}] for the locale: {}", key, locale);
      }
    } finally {
      this.lock.readLock().unlock();
    }
    return Optional.empty();
  }

  /**
   * Add a locale among other locales. This loads all resource files that were previously added with specified locale and all resource files added later will be loaded with this new locale as well.
   *
   * @param locale Locale you want to add.
   * @return This instance.
   */
  public @NotNull PolyglotMultiResourceBundle loadLocale(final @NotNull Locale locale) {
    log.trace("loadLocale(locale = [{}])", locale);
    this.lock.writeLock().lock();
    try {
      if (this.locales.containsKey(locale)) {
        return this;
      }
      SingleLocaleMultiResourceBundle singleLocaleMultiResourceBundle =
          new SingleLocaleMultiResourceBundle(locale);
      singleLocaleMultiResourceBundle.addMessageSources(this.resourceBundleDescriptors.toArray(new ResourceBundleDescriptor[0]));
      this.locales.put(locale, singleLocaleMultiResourceBundle);
    } finally {
      this.lock.writeLock().unlock();
    }
    return this;
  }

  /**
   * Retrieve singleton instance of this class.
   *
   * @return Singleton instance.
   */
  public static @NotNull PolyglotMultiResourceBundle getSingleton() {
    log.trace("getSingleton()");
    PolyglotMultiResourceBundle localSingleton = singleton;
    if (localSingleton == null) {
      synchronized (singletonLock) {
        localSingleton = singleton;
        if (localSingleton == null) {
          localSingleton = new PolyglotMultiResourceBundle();
          singleton = localSingleton;
        }
      }
    }
    return localSingleton;
  }

  @Override
  public @NotNull MultiResourceBundle addMessageSources(final @Nullable ResourceBundleDescriptor... resourceBundleDescriptors) {
    log.trace("addMessageSources(resourceBundleDescriptors = [{}])", (Object[]) resourceBundleDescriptors);
    if (resourceBundleDescriptors == null) {
      return this;
    }
    this.lock.writeLock().lock();
    try {
      this.locales.forEach((k, v) -> v.addMessageSources(resourceBundleDescriptors));
      Arrays.stream(resourceBundleDescriptors).filter(Objects::nonNull).forEach(this.resourceBundleDescriptors::add);
    } finally {
      this.lock.writeLock().unlock();
    }
    return this;
  }

  /**
   * Retrieve a list of loaded locales.
   *
   * @return Set of locales that this instance has already loaded.
   */
  public @NotNull Set<Locale> getLocales() {
    log.trace("getLocales()");
    return this.locales.keySet();
  }

  public @NotNull PolyglotMultiResourceBundle setDefaultLocale(final @NotNull Locale defaultLocale) {
    log.trace("setDefaultLocale(defaultLocale = [{}])", defaultLocale);
    this.defaultLocale = defaultLocale;
    return this;
  }
}
