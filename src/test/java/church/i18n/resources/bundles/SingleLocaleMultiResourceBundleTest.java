/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.resources.bundles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import org.junit.jupiter.api.Test;

class SingleLocaleMultiResourceBundleTest {

  @Test
  void testLocale() {
    SingleLocaleMultiResourceBundle multiBundle = new SingleLocaleMultiResourceBundle(
        Locale.FRANCE);
    assertEquals(Locale.FRANCE, multiBundle.getLocale());
  }

  @Test
  void addMessageSources_null() {
    SingleLocaleMultiResourceBundle multiBundle = new SingleLocaleMultiResourceBundle(
        Locale.FRANCE);
    multiBundle.addMessageSources((String) null);
    multiBundle.addMessageSources((String[]) null);
    Enumeration<String> emptyKeys = multiBundle.getKeys();
    assertIterableEquals(Collections.list(emptyKeys), Collections.list(multiBundle.getKeys()));
  }

  @Test
  void addMessageSources_single() {
    SingleLocaleMultiResourceBundle multiBundle = new SingleLocaleMultiResourceBundle(
        Locale.FRANCE);
    multiBundle.addMessageSources("i18n.A");
    assertEquals("appartement", multiBundle.getObject("msg-A1"));
  }

  @Test
  void addMessageSources_multiple() {
    SingleLocaleMultiResourceBundle multiBundle = new SingleLocaleMultiResourceBundle(
        Locale.FRANCE);
    multiBundle.addMessageSources("i18n.A", "i18n.B");
    assertEquals("appartement", multiBundle.getObject("msg-A1"));
    assertEquals("capuchon", multiBundle.getObject("msg-B1"));
  }

  @Test
  void handleGetObject_nonExistingKey_noBundle() {
    SingleLocaleMultiResourceBundle multiBundle = new SingleLocaleMultiResourceBundle(
        Locale.FRANCE);
    assertThrows(MissingResourceException.class, () -> multiBundle.getObject("non-existing-key"));
  }

  @Test
  void handleGetObject_nonExistingKey_withBundle() {
    SingleLocaleMultiResourceBundle multiBundle = new SingleLocaleMultiResourceBundle(
        Locale.FRANCE);
    multiBundle.addMessageSources("i18n.A");
    assertThrows(MissingResourceException.class, () -> multiBundle.getObject("non-existing-key"));
  }

  @Test
  void handleGetObject_existingKey_singleBundle() {
    SingleLocaleMultiResourceBundle multiBundle = new SingleLocaleMultiResourceBundle(
        Locale.FRANCE);
    multiBundle.addMessageSources("i18n.A");
    assertEquals("apéritif", multiBundle.getObject("msg"));
  }

  @Test
  void handleGetObject_existingKey_multiBundle() {
    SingleLocaleMultiResourceBundle multiBundle = new SingleLocaleMultiResourceBundle(
        Locale.SIMPLIFIED_CHINESE);
    multiBundle.addMessageSources("i18n.A", "i18n.B");
    assertEquals("发夹", multiBundle.getObject("msg-B1"));
  }

  @Test
  void getKeys() {
    SingleLocaleMultiResourceBundle multiBundle = new SingleLocaleMultiResourceBundle(
        Locale.FRANCE);
    multiBundle.addMessageSources("i18n.A", "i18n.B");

    ArrayList<String> keys = Collections.list(multiBundle.getKeys());
    Collections.sort(keys);

    assertEquals(List.of("msg", "msg-A1", "msg-A2", "msg-B1"), keys);
  }
}
