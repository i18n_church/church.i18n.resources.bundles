/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.resources.bundles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class PolyglotMultiResourceBundleTest {

  private static final Object[] EMPTY_PARAMS = new Object[0];

  @Test
  void singleton() {
    assertSame(PolyglotMultiResourceBundle.getSingleton(),
        PolyglotMultiResourceBundle.getSingleton());
  }

  @Test
  void constructor_nullLocaleThrowsException() {
    PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle(
        (Locale) null, "i18n.A");
    assertEquals(Set.of(Locale.ROOT, Locale.getDefault()), bundle.getLocales());
  }

  @Test
  void getStringDefaultLocale() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle((String[]) null);
      bundle.addMessageSources("i18n.A", "i18n.B");
      MessageFormat msg = bundle.getString("msg-B1", Collections.emptyList())
          .orElseThrow();
      assertEquals("发夹", msg.format(EMPTY_PARAMS));
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void getStringDefaultLocale_nullKey() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle("i18n.A", "i18n.B");
      assertTrue(bundle.getString(null, Collections.emptyList()).isEmpty());
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void getStringDefaultLocale_nullKey_nullLocale() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle("i18n.A", "i18n.B");
      assertTrue(bundle.getString(null, (Locale) null).isEmpty());
      assertTrue(bundle.getString(null, (Enumeration<Locale>) null).isEmpty());
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void getStringWithLocale_initWithNonExisting() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle(Locale.CANADA, "i18n.A",
          "i18n.B");
      MessageFormat msg = bundle.getString("msg-B1", Locale.FRANCE).orElseThrow();
      assertEquals("capuchon", msg.format(EMPTY_PARAMS));
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void getStringWithLocale_prioritiezedLocale_nullKey() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle(Locale.CANADA, "i18n.A",
          "i18n.B");
      List<Locale> locales = List.of(Locale.FRANCE, Locale.SIMPLIFIED_CHINESE, Locale.CANADA);
      assertTrue(bundle.getString(null, locales).isEmpty());
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void getStringWithLocale_nullKey() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle(Locale.CANADA, "i18n.A",
          "i18n.B");
      assertTrue(bundle.getString(null, Locale.FRANCE).isEmpty());
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void getStringPrioritizedLocale() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle("i18n.A", "i18n.B");
      MessageFormat msg = bundle
          .getString("msg-B1", List.of(Locale.CANADA_FRENCH, Locale.FRANCE)).orElseThrow();
      assertEquals("capuchon", msg.format(EMPTY_PARAMS));
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void getStringEnumeratedLocale() {
    Locale defaultLocale = Locale.getDefault();
    Enumeration<Locale> enumLocale = Collections
        .enumeration(List.of(Locale.CANADA_FRENCH, Locale.FRANCE));
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle("i18n.A", "i18n.B");
      MessageFormat msg = bundle.getString("msg-B1", enumLocale).orElseThrow();
      assertEquals("capuchon", msg.format(EMPTY_PARAMS));
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void getStringEnumeratedLocale_emptyEnumerationGetDefault() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle("i18n.A", "i18n.B");
      MessageFormat messageFormat = bundle.getString("msg-B1", Collections.emptyEnumeration())
          .orElseThrow();
      assertEquals("发夹", messageFormat.format(EMPTY_PARAMS));
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void getStringEnumeratedLocale_initNonExisting_emptyEnumerationRootLocaleExist() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.CANADA);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle("i18n.A", "i18n.B");
      assertFalse(bundle.getString("msg-B1", Collections.emptyEnumeration()).isEmpty());
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void addResourceFileName_toEmptyBundle() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle();
      bundle.addMessageSources("i18n.A");
      MessageFormat msg = bundle.getString("msg", Collections.emptyList()).orElseThrow();
      assertEquals("开胃菜", msg.format(EMPTY_PARAMS));
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void addMessageSources_null() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle();
      bundle.addMessageSources((String) null);
      bundle.addMessageSources("i18n.A");
      MessageFormat msg = bundle.getString("msg", Collections.emptyList()).orElseThrow();
      assertEquals("开胃菜", msg.format(EMPTY_PARAMS));
      bundle.addMessageSources((String[]) null);
      msg = bundle.getString("msg", Collections.emptyList()).orElseThrow();
      assertEquals("开胃菜", msg.format(EMPTY_PARAMS));
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void addResourceFileName_toEmptyBundle_autoLoadLocale() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle();
      bundle.addMessageSources("i18n.A");
      MessageFormat msg = bundle.getString("msg", Locale.ENGLISH).orElseThrow();
      assertEquals("appetizer-english", msg.format(EMPTY_PARAMS));
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void addResourceFileName_toEmptyBundle_autoLoadMultiLocale() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle();
      bundle.addMessageSources("i18n.A");
      MessageFormat msg = bundle.getString("msg",
          List.of(Locale.CANADA, Locale.ITALIAN, Locale.FRANCE, Locale.ENGLISH)).orElseThrow();
      assertEquals("apéritif", msg.format(EMPTY_PARAMS));
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void addResourceFileName_toEmptyBundle_noMessage() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle();
      bundle.addMessageSources("i18n.A");
      assertTrue(bundle.getString("msg-non-existing", Collections.emptyList()).isEmpty());
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void addResourceFileName_toEmptyBundle_noMessage_autoLoadLocale() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle();
      bundle.addMessageSources("i18n.A");
      assertTrue(bundle.getString("msg-non-existing", Locale.ENGLISH).isEmpty());
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void addResourceFileName_toEmptyBundle_noMessage_autoLoadMultiLocale() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle();
      bundle.addMessageSources("i18n.A");
      assertTrue(bundle.getString("msg-non-existing",
          List.of(Locale.ENGLISH, Locale.FRENCH, Locale.CANADA)).isEmpty());
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void loadLocale_toEmptyBundle() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle();
      bundle.loadLocale(Locale.FRENCH);
      assertEquals(Set.of(Locale.ROOT, Locale.SIMPLIFIED_CHINESE, Locale.FRENCH),
          bundle.getLocales());
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void loadLocale_toLoadedSingleDefaultBundle() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle("i18n.A");
      bundle.loadLocale(Locale.FRENCH);
      assertEquals(Set.of(Locale.ROOT, Locale.SIMPLIFIED_CHINESE, Locale.FRENCH),
          bundle.getLocales());
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void loadLocale_toLoadedSingletBundle() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle(
          Locale.TRADITIONAL_CHINESE, "i18n.A");
      bundle.loadLocale(Locale.FRENCH);
      assertEquals(Set.of(Locale.ROOT, Locale.TRADITIONAL_CHINESE, Locale.FRENCH),
          bundle.getLocales());
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void loadLocale_loadMultipleTimes() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle("i18n.A");
      bundle.loadLocale(Locale.SIMPLIFIED_CHINESE);
      bundle.loadLocale(Locale.SIMPLIFIED_CHINESE);
      assertEquals(Set.of(Locale.ROOT, Locale.SIMPLIFIED_CHINESE), bundle.getLocales());
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void setDefaultLocale() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.FRANCE);
      //it is created with default system locale
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle();
      bundle.addMessageSources("i18n.A");
      MessageFormat msg = bundle.getString("msg", List.of()).orElseThrow();
      assertEquals("apéritif", msg.format(EMPTY_PARAMS));

      //and it reflects change of default locale
      assertNotNull(bundle.setDefaultLocale(Locale.SIMPLIFIED_CHINESE));
      msg = bundle.getString("msg", List.of()).orElseThrow();
      assertEquals("开胃菜", msg.format(EMPTY_PARAMS));
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void getString_nullParams() {
    PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle();
    assertTrue(bundle.getString(null, (Locale) null).isEmpty());
    assertTrue(bundle.getString(null, Locale.CANADA).isEmpty());
    assertTrue(bundle.getString(null, (Enumeration<Locale>) null).isEmpty());
    assertTrue(bundle.getString(null, (List<Locale>) null).isEmpty());
    assertTrue(bundle.getString(null, List.of()).isEmpty());
  }

  @Test
  void singletonTest() {
    assertNotNull(PolyglotMultiResourceBundle.getSingleton(), "Singleton not created!");
    assertSame(PolyglotMultiResourceBundle.getSingleton(),
        PolyglotMultiResourceBundle.getSingleton(), "Singleton is not the same instance");
  }

  @Test
  void loadLocale_twice() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.FRANCE);
      PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle();
      bundle.addMessageSources("i18n.A");

      Set<Locale> originalLocales = bundle.getLocales();
      assertNotNull(bundle.loadLocale(Locale.SIMPLIFIED_CHINESE));
      assertNotNull(bundle.loadLocale(Locale.SIMPLIFIED_CHINESE));
      assertNotNull(bundle.loadLocale(Locale.UK));
      assertNotNull(bundle.loadLocale(Locale.FRANCE));
      assertNotNull(bundle.loadLocale(Locale.FRANCE));
      assertNotNull(bundle.loadLocale(Locale.SIMPLIFIED_CHINESE));

      Set<Locale> expectedLocales = new HashSet<>(originalLocales);
      expectedLocales.add(Locale.SIMPLIFIED_CHINESE);
      expectedLocales.add(Locale.UK);
      assertEquals(expectedLocales, originalLocales);

    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void addNullMessageSources() {
    //test that it does not throw an exception...
    PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle();
    assertNotNull(bundle.addMessageSources((String) null));
    assertNotNull(bundle.addMessageSources((String[]) null));
    assertNotNull(bundle.addMessageSources(null, "i18n.A"));
    assertNotNull(bundle.addMessageSources(null, "i18n.A", null));
    assertNotNull(bundle.addMessageSources(new String[]{null}));
  }

  @Tag("PIT")
  @ParameterizedTest
  @MethodSource("constructors")
  @Timeout(value = 500, unit = TimeUnit.MILLISECONDS)
  void lockWasReleased(final PolyglotMultiResourceBundle bundle) {
    List<Thread> threads = List.of(
        new Thread(() -> bundle.loadLocale(Locale.SIMPLIFIED_CHINESE)),
        new Thread(() -> bundle.addMessageSources("i18n.A")),
        new Thread(() -> bundle.addMessageSources("i18n.A", "i18n.B")),
        new Thread(bundle::getLocales),
        new Thread(() -> bundle.setDefaultLocale(Locale.TRADITIONAL_CHINESE)),
        new Thread(() -> bundle.getString("msg-A2", Locale.UK)),
        new Thread(() -> bundle
            .getString("msg-A2", List.of(Locale.UK, Locale.US, Locale.SIMPLIFIED_CHINESE))),
        new Thread(() -> bundle.getString("msg-A2",
            Collections.enumeration(List.of(Locale.UK, Locale.US, Locale.SIMPLIFIED_CHINESE))))
    );
    threads.forEach(Thread::start);
    threads.forEach(t -> {
      try {
        t.join(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    });
  }

  private static Stream<Arguments> constructors() {
    return Stream.of(
        Arguments.of(new PolyglotMultiResourceBundle(Locale.FRENCH, (String[]) null)),
        Arguments.of(new PolyglotMultiResourceBundle(Locale.FRENCH, "i18n.A")),
        Arguments.of(new PolyglotMultiResourceBundle(Locale.FRENCH, "i18n.A", "i18n.B")),
        Arguments.of(new PolyglotMultiResourceBundle()),
        Arguments.of(new PolyglotMultiResourceBundle("i18n.A")),
        Arguments.of(new PolyglotMultiResourceBundle("i18n.A", "i18n.B"))
    );
  }

}
