# Multi-Resource Bundle

The default implementation of ResourceBundle in Java provides a functionality to load a single
resource file with a single locale. This may become a limitation when you have your messages stored
in multiple files and your application needs to access all of them.

When you develop a web application and you have a localized application, if your backend returns
localized messages, you need to handle multiple resource bundles with different locales.

The library provides support for such cases so you don't need to handle them manually.

```java
SingleLocaleMultiResourceBundle multiBundle = 
    new SingleLocaleMultiResourceBundle(Locale.FRANCE, "i18n.A", "i18n.B");

assertEquals("appartement", multiBundle.getObject("msg-A1"));
assertEquals("capuchon", multiBundle.getObject("msg-B1"));
```

Resource bundles:
`i18n.A: msg-A1=appartement`
`i18n.B: msg-B1=capuchon`

## Multiple locales

When the application receives a request, typical header contains list of locales possibly also with
priorities. In this case the user expects that server will respect them and returns the best
response according to the request. Example header may look like:
> Accept-Language: fr-CH, fr;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5

In this case, the user expects the response to be in French (Swiss), French, English or German. The
library provides such functionality through `PolyglotMultiResourceBundle` and you can directly
specify the prioritized list of languages. The first message found in the languages order is
returned and used. The library also loads localization files lazily, so only in case the locale is
needed, it tries to load it first. In case it does not exist, it will try to load the message in the
default locale of the server or with the root locale.

```java
Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle("i18n.A");
assertEquals("apéritif", bundle.getString("msg", List.of(Locale.CANADA, Locale.ITALIAN, Locale.FRANCE, Locale.ENGLISH)).orElseThrow());
```

Available locales:

1) `A.properties`
2) `A_en.properties`
3) `A_en_GB.properties`
4) `A_en_US.properties`
5) `A_fr_FR.properties`
6) `A_zh_CN.properties`
7) `A_zh_TW.properties`

The `PolyglotMultiResourceBundle` cannot find Canadian and Italian locales, however, it finds French
so the message returned is the French translation of the word *aperitif*.
